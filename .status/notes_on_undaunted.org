# -*- mode: org -*-
#+SETUPFILE: ~/.vault/cfgs/emacs/apps/org/org-setupfile
#+TITLE: notes on project // Undaunted

#+EXPORT_FILE_NAME: public/index.html

* synopsis
* advisement                                                       :noexport:
- An uncommited =.encrc= is used to variablized this project in support of greater generalizability and OpSec.
  #+CAPTION: An Obscured Example, for Didactic Purposes
  #+begin_example
#!/bin/false

##
export project_root=$(realpath .)
export root_relapath="."

##
export OCI_IMAGE_V00=<org>/<image>
export OCI_IMAGE_V01=<org>/<image>
#
export OCI_IMAGE_ORIG=${OCI_IMAGE_V00}
export OCI_IMAGE_MODIFIED=${OCI_IMAGE_V01}

##
export uri_endpoint=<uri>
export count_key=<key_id>

## aws-vault
export aws_vault__profile=""

## // backend // remote
export TF_VAR_workspace="lab"
export TF_VAR_backend_tfc_organization="<tf_org>"
export TF_VAR_backend_tfc_workspace_prefix="undaunted"
export TF_VAR_backend_tfc_backend_token="<token>"
export TF_VAR_backend_tfc_hostname="app.terraform.io"
export TF_VAR_required_version=">= 0.13.5"

## // provider // aws
export AWS_DEFAULT_REGION="<aws_region>"


## EOF
  #+end_example
* TODO tasklist                                                    :noexport:
* chapters                                                         
** Introduction to Undaunted DevOps Project
   :PROPERTIES:
   :ID:       7614A80B-9C69-403E-8695-5D84A1AC7F08
   :END:
[2020-11-14 Sat 13:48]

- context ::
  - [6/10] overview
    - [X] A public repo, containing the following requirements and assets ([[https://gitlab.com/nickgarber/undaunted][here]])
    - [X] A script that can run within the provided Docker image ([[file:assets/exec_script/bash__curl_getcount.bash][here]])
    - [X] ... the shell script must return the integer count only
    - [X] A solution.md document to communicate design
    - [ ] ... the solution doc articulates how to deploy via pipeline to Fargate ([[*howto, Deploy a Simple Smoke Test Container-Image to Fargate via Pipeline][here]])
    - [X] ... the solution doc includes any other future updates, changes ([[* howto, Enact Supplimental OpSec and Quality Controls Auto][here]])
    - [X] ... the solution doc articulates outstanding code you would add or change
    - [ ] ... the solution doc lists any assumptions, changes, or details of the implementation that materially impact the addition of future features. ([[* Requirements Breakdown][here]])
    - [-] In support of the communication objectives, feel free to use other supplimental methods to representing or codifying your ideas ([[https://nickgarber.gitlab.io/undaunted][here]])
    - [-] Be prepared to defend and explain the approach and implementation used in detail. (:+1:)

  - info/notes
    - srcs
      #+NAME: 7614A80B-9C69-403E-8695-5D84A1AC7F08-srcs
      #+CAPTION: reference list
      | ! | title                    | url                                                                                                             | desc | rowid                                    |
      |   | title                    | url                                                                                                             | desc | rowid                                    |
      |---+--------------------------+-----------------------------------------------------------------------------------------------------------------+------+------------------------------------------|
      | / |                          |                                                                                                                 |      | <7>                                      |
      | # | Undaunted DevOps Project | [[file:docs/initial_request.txt]]                                                                                                                    |      | <<2C571A5D-3491-4B7E-9821-E0A2BFE28436>> |
      | # |                          | https://gmusumeci.medium.com/how-to-deploy-aws-ecs-fargate-containers-step-by-step-using-terraform-545eeac743be |      | <<E22004D1-C418-4258-9DBF-7C2FACE1D485>> |
      | # |                          | https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_service                         |      | <<4109B55B-71E9-43F7-8301-9170CC7A9DFA>> |
      #+TBLFM: $5='(if (and (string= @1$rowid "rowid") (string= $rowid "")) (concat "<<"(org-id-new) ">>") $rowid)

    - on Initial Understanding a/o Discovery

    - on Seconday Objectives
      - Demonstrate mastery of CI/CD concepts and implementations relating to Fargate
      - the rest of the tasks are to pick your knowledge of complete deployments

  - Questions
    - Q: does the wording of req6.4 preclude using an updated container with the smoke-test app baked in?
      - It would be preferable to seal and ship the complete runtime into a downstream-derived container
      - do you think they mean as a base container that can be modified or as an unmodified container?
      - if it needs to be unmodified I'll need to parse the result in bash, b/c python, ruby, jq, etc aren't installed in the image
    - Q: it also doesn't say if I should actually deploy this ... does it?
      - It asks that I describe how I would but I dont see anything about how it would be enabled ... no?
      - so the ci/cd you use needs to be able to push to fargate
      - In a solution.md in the root of the repository, design a pipeline to deploy the counting application to AWS Fargate
      - The deployment pipeline part is probably what they most want to see
      - so then, so is it all chalkboard work or actual working deployment?

  - log

*** Initial Request
:PROPERTIES:
:ID:       23502E95-7C63-4B80-9B15-4DC2EBB90D73
:END:
#+begin_src org :tangle public/initial_request.txt :comments link
*Undaunted DevOps Project*

1. You've been asked to create a series of proofs of concept that supports the customers public health and social justice initiatives.
2. You have been assigned to build supporting infrastructure for a proof concept.
3. The team has carved out two hours to complete this proof of concept. If you finish in less, time, great!
4. But because the requirements are evolving rapidly, to avoid any rework, don’t spend much more than two hours on this project.

5. The proof of concept created by your team will be a web-application that runs in a Docker and functions similar to the API deployed to https://countapi.xyz/.

6. Your assignment is to create supporting scripts and a pipeline.

   1. Create a simple smoke test script: Write a bash command to request and parse out the total number of hits for the key below using the https://countapi.xyz/ api.
   2. Key: <xx-redacted-keyname-xx>
   3. Must write only the integer representing the number of hits
   4. The command must be able to run inside <xx-redacted-container-xx>.

7. Design a deployment pipeline: In a solution.md in the root of the repository, design a pipeline to deploy the counting application to AWS Fargate

   1. List any future updates, changes, or outstanding code you would like to add or would recommend be added
   2. Document any assumptions, changes, or details of the implementation that materially impact the addition of future features.
   3. Feel free to use any method of representing or codifying your ideas
   4. Be prepared to spend 15 to 30 minutes discussing your approach and implementation with another DevOps Engineer.

8. Other Details
   1. All artifacts could be committed to a public git repository (e.g. GitHub.com) /and/ submitted in the response.
#+end_src

*** Requirements Breakdown
:PROPERTIES:
:ID: 61B051C5-507C-46FD-9A1F-D9FD7AEBE1F1
:END:
[2020-11-14 Sat 16:04]

- context ::
  - [/] overview
  - info/notes
    - srcs
      #+NAME: 61B051C5-507C-46FD-9A1F-D9FD7AEBE1F1-srcs
      #+CAPTION: reference list
      | ! | title                                                                                                                                                            | url    | desc                                                                                            | rowid                                    |
      |   | title                                                                                                                                                            | url    | desc                                                                                            | rowid                                    |
      |---+------------------------------------------------------------------------------------------------------------------------------------------------------------------+--------+-------------------------------------------------------------------------------------------------+------------------------------------------|
      | / |                                                                                                                                                                  |        |                                                                                                 | <7>                                      |
      | # | You've been asked to create a series of proofs of concept that support the customer                                                                              | req1   |                                                                                                 | <<D10929D7-932E-4322-8DC9-A240744918AA>> |
      | # | You have been assigned to build supporting infrastructure for a proof concept.                                                                                   | req2   |                                                                                                 | <<45421E10-DEE0-4CF3-9EEB-4983B6FFE7EF>> |
      | # | The team has carved out two hours to complete this proof of concept. If you finish in less time, great!                                                          | req3   |                                                                                                 | <<50BF104C-D2C0-457F-94C9-E07AAEAE9396>> |
      | # | Because the requirements are evolving rapidly, to avoid any rework, don’t spend much more than two hours on this project.                                        | req4   | work effort must be less than 2 hours                                                           | <<1AD5AB10-CAEA-49CF-9BFF-DF6DED10B719>> |
      | # | The proof of concept created by your team will be a web-application that runs in a Docker and functions similar to the API deployed to https://countapi.xyz/.    | req5   | this references the combined/team goal, not the individual goal, so doesn't impose requirements | <<DA475D3B-2776-48E7-8876-E99FD93F8FE1>> |
      | # | Your assignment is to create supporting scripts and a pipeline.                                                                                                  | req6   |                                                                                                 | <<81B0FEF5-5FD5-4D14-9068-C2319CA1F389>> |
      | # | Create a simple smoke test script: Write a bash command to request and parse out the total number of hits for the key below using the https://countapi.xyz/ api. | req6.1 |                                                                                                 | <<133BC853-0A58-47E5-99F4-674EEA0EE76D>> |
      | # | Key: <xx-redacted-keyname-xx>                                                                                                                                    | req6.2 |                                                                                                 | <<91F72436-8F8C-4417-9129-1CF4EC8AB11E>> |
      | # | Must write only the integer representing the number of hits                                                                                                      | req6.3 |                                                                                                 | <<49AC6E9B-BCDB-4BA7-8AD0-52A6514870F3>> |
      | # | The command must be able to run inside <xx-redacted-container-xx>.                                                                                               | req6.4 |                                                                                                 | <<8E00E16D-C5A9-45EB-8BC7-77531EB411A2>> |
      | # | Design a deployment pipeline: In a solution.md in the root of the repository, design a pipeline to deploy the counting application to AWS Fargate                | req7   |                                                                                                 | <<B1EEBB11-C087-454B-8FA3-9FF9AD844E20>> |
      | # | List any future updates, changes, or outstanding code you would like to add or would recommend be added                                                          | req7.1 |                                                                                                 | <<7E8701ED-320C-40BA-B377-9AD8110226B2>> |
      | # | Document any assumptions, changes, or details of the implementation that materially impact the addition of future features.                                      | req7.2 |                                                                                                 | <<495CB56C-C5A5-4DDD-8EDA-345FE37070FA>> |
      | # | Feel free to use any method of representing or codifying your ideas                                                                                              | req7.3 |                                                                                                 | <<BDBEF241-37EA-4E96-9FC1-344AA1531C2A>> |
      | # | Be prepared to spend 15 to 30 minutes discussing your approach and implementation with another DevOps Engineer.                                                  | req7.4 |                                                                                                 | <<00E7EA15-7DE2-47FE-8A5A-13BC67A5EBC1>> |
      | # | Other Details                                                                                                                                                    | req8   |                                                                                                 | <<2F8ABFDE-F7FA-4846-B3CF-25335E708D92>> |
      | # | All artifacts could be committed to a public git repository (e.g. GitHub.com) /and/ submitted in the response.                                                   | req8.1 |                                                                                                 | <<55D3E6E9-5FDB-4A64-9DC7-21CFEE2D54BA>> |
      #+TBLFM: $5='(if (and (string= @1$rowid "rowid") (string= $rowid "")) (concat "<<"(org-id-new) ">>") $rowid)

    - on Request Breakdown by Requirements
      - <<<req1>>> You've been asked to create a series of proofs of concept that supports the customer.
      - <<<req2>>> You have been assigned to build supporting infrastructure for a proof concept.
      - <<<req3>>> The team has carved out two hours to complete this proof of concept. If you finish in less time, great!
      - <<<req4>>> Because the requirements are evolving rapidly, to avoid any rework, don’t spend much more than two hours on this project.
      - <<<req5>>> The proof of concept created by your team will be a web-application that runs in a Docker and functions similar to the API deployed to https://countapi.xyz/.
      - <<<req6>>> Your assignment is to create supporting scripts and a pipeline.
        - <<<req6.1>>> Create a simple smoke test script: Write a bash command to request and parse out the total number of hits for the key below using the https://countapi.xyz/ api.
        - <<<req6.2>>> Key: <xx-redacted-keyname-xx>
        - <<<req6.3>>> Must write only the integer representing the number of hits
        - <<<req6.4>>> The command must be able to run inside <xx-redacted-conainter-xx>.
      - <<<req7>>> Design a deployment pipeline: In a solution.md in the root of the repository, design a pipeline to deploy the counting application to AWS Fargate
        - <<<req7.1>>> List any future updates, changes, or outstanding code you would like to add or would recommend be added
        - <<<req7.2>>> Document any assumptions, changes, or details of the implementation that materially impact the addition of future features.
        - <<<req7.3>>> Feel free to use any method of representing or codifying your ideas
        - <<<req7.4>>> Be prepared to spend 15 to 30 minutes discussing your approach and implementation with another DevOps Engineer.
      - <<<req8>>> Other Details
        - <<<req8.1>>> All artifacts could be committed to a public git repository (e.g. GitHub.com) /and/ submitted in the response.

** Solution: Simple Smoke Test Script with Deployment to Fargate
*** howto, Query a Web-service API for 'count' using a Simple Smoke Test Script in the provided Container-Image 

- procedure :: Return an integer count using the provided container-image
  - Nota Bene
    - This isn't a particularly elegant or favorable approach and is presented solely to demonstrate an option that dogmatically adheres to requirement req6.4.
    - The purpose of a docker container is to create and seal a runtime, then make it portable.
    - The runtime in the provided container-image is generic and not particularly well-suited to this task.
    - For a more operationally favorable approach, (a customized container-image), see 
    - In order to accomplish this portion of the exercise goal in strict adherence with the requi
    
  - Run the Container-Image from CLI to demonstrate and verify
    #+begin_src bash :wrap EXAMPLE :output drawer :results output :file-coding utf-8-unix :var oci_image=(getenv "OCI_IMAGE_ORIG")
      docker run -t --rm -v $(pwd)/assets/exec_script/bash__curl_getcount.bash:/run.sh ${oci_image} /run.sh
    #+end_src

    #+RESULTS:
    #+begin_EXAMPLE
    1
    #+end_EXAMPLE

*** howto, Query a Web-service API for 'count' using a Simple Smoke Test Script in an adapted Container-Image 
:PROPERTIES:
:ID:       545E5059-BC2E-4775-B94F-A1091C658EAA
:END:
- procedure :: Return an integer count using a customized container-image

  - /(ex.)/ build new container with the script embedded
    #+begin_src dockerfile
# -*- docker-image-name: "adapted_but_undaunted" -*-
FROM debian:testing AS build_envkit_python
SHELL ["/bin/bash", "-l", "-c"]
ENV DEBIAN_FRONTEND noninteractive
RUN apt -y update && apt install -y curl git 
RUN curl https://pyenv.run | bash && touch ~/.bashrc && echo export PATH="${HOME}/.pyenv/bin:${PATH}" >> ~/.bashrc && ~/.pyenv/bin/pyenv init - >> ~/.bashrc
WORKDIR /build
ENV PYENV_VERSION 3.9.0
RUN apt install -y gcc make libssl-dev zlib1g-dev libncurses5-dev libncursesw5-dev libreadline-dev libsqlite3-dev libgdbm-dev libdb5.3-dev libbz2-dev libexpat1-dev liblzma-dev libffi-dev uuid-dev libbz2-dev
RUN pyenv install ${PYENV_VERSION} && pyenv local ${PYENV_VERSION}
RUN pyenv exec pip install pipenv


##
FROM build_envkit_python AS build_app_example
WORKDIR /build
ADD Pipfile /build/
ADD assets /build/
RUN pyenv exec pip install pipenv
RUN pyenv exec pipenv run tiamat build
RUN chmod +x /build/app



##
FROM alpine:latest AS deploy_app_example
COPY --from=build_app_example /build/app /run
ENTRYPOINT ["/run/app"]


## EOF
    #+end_src

  - /(ex.)/ Run the Mininal and Self-Contained Container-Image from CLI to demonstrate and verify
    #+begin_src bash :wrap EXAMPLE :output drawer :results output :file-coding utf-8-unix :var oci_image=(getenv "OCI_IMAGE_MODIFIED")
      docker run -t --rm  ${oci_image}
    #+end_src

*** howto, Deploy a Simple Smoke Test Container-Image to Fargate via Pipeline
- procedure :: /(wip)/ Deploy a Container in EKS/Fargate using AWS CLI in Gitlab CI
  - Nota Bene
    - https://docs.gitlab.com/ee/ci/cloud_deployment/#run-aws-commands-from-gitlab-cicd
  - /(ex.)/ Imperatively Configure and Launch a Container to 

- procedure :: /(wip)/ Deploy a Container in EKS/Fargate using Terraform in Gitlab CI

** Future Enhancements
*** howto, Re-Write the Simple Smoke Test Script in a More Robust Language
:PROPERTIES:
:ID:       CC08573C-18E8-4C05-A9E0-3D8A12518076
:END:
- procedure :: Re-write the Smoke Test Script as a Python App
  - Nota Bene
    - https://pypi.org/project/count-api/
  - create alternate executable
    #+begin_src python :python "pipenv run python" :results output :wrap example :output drawer :tangle assets/exec_script/py3__getcount.py :comments link
#!/usr/bin/env python3
import os
import requests

uri_endpoint = os.environ["uri_endpoint"]
count_key = os.environ["count_key"]
# Q&D
print((requests.get(f"{uri_endpoint}/{count_key}", headers={"Accept": "application/json"})).json()["value"])

    #+end_src

    #+RESULTS:
    #+begin_example
    1
    #+end_example





*** howto, Create a Multi-Stage Container-Image Build that produces a Single Executatble in a Minimal Runtime
:PROPERTIES:
:ID:       545E5059-BC2E-4775-B94F-A1091C658EAA
:END:
- procedure :: 
  - build new multi-stage container
    #+begin_src dockerfile :tangle assets/dockerfile/Dockerfile :comments link
# -*- docker-image-name: "adapted_but_undaunted" -*-
FROM debian:testing AS build_envkit_python
SHELL ["/bin/bash", "-l", "-c"]
ENV DEBIAN_FRONTEND noninteractive
RUN apt -y update && apt install -y curl git 
RUN curl https://pyenv.run | bash && touch ~/.bashrc && echo export PATH="${HOME}/.pyenv/bin:${PATH}" >> ~/.bashrc && ~/.pyenv/bin/pyenv init - >> ~/.bashrc
WORKDIR /build
ENV PYENV_VERSION 3.9.0
RUN apt install -y gcc make libssl-dev zlib1g-dev libncurses5-dev libncursesw5-dev libreadline-dev libsqlite3-dev libgdbm-dev libdb5.3-dev libbz2-dev libexpat1-dev liblzma-dev libffi-dev uuid-dev libbz2-dev
RUN pyenv install ${PYENV_VERSION} && pyenv local ${PYENV_VERSION}
RUN pyenv exec pip install pipenv


##
FROM build_envkit_python AS build_app_example
WORKDIR /build
ADD Pipfile /build
ADD assets
RUN pyenv pipenv install ${PIPENV_INSTALL_MODS}
RUN pyenv pipenv run tiamat build
RUN chmod +x /build/app



##
FROM alpine:latest AS deploy_app_example
COPY --from=build_app_example /build/app /run
ENTRYPOINT ["/run/app"]


## EOF
    #+end_src

  - /(wip)/ Run the Mininal and Self-Contained Container-Image from CLI to demonstrate and verify
    #+begin_src bash :wrap EXAMPLE :output drawer :results output :file-coding utf-8-unix :var oci_image=(getenv "OCI_IMAGE_MODIFIED")
      docker run -t --rm  ${oci_image}
    #+end_src

*** howto, Enact Supplimental OpSec and Quality Controls Auto
- procedure :: /(wip)/ Enhanced CI in the Local Repo using Pre-Commit

- procedure :: /(wip)/ Enhanced CI in the Git Repo

- procedure :: /(wip)/ Enhanced CI in the Container-Image Registry


* extrefs                                                          :noexport:
** reports
*** clock summary this day
   #+BEGIN: clocktable :maxlevel 4 :scope file :block today :emphasize :link
   #+END:
*** clock summary recent weeks
   #+BEGIN: clocktable :scope file :maxlevel 4 :tstart "<-2w>" :tend "<now>" :step day :stepskip0 :emphasize :link
   #+END:
*** todo tasks status report
   #+BEGIN:  columnview :id '(use ID from TODO properties) :skip-empty-rows t :maxlevel 3 :hlines 2 :vlines nil
   #+END:
** defined-terms
** local-REFILE
** archived-items                                                   :ARCHIVE:
** footnotes
[fn:0] simple example of a footnote, delete at your leisure
