
# Table of Contents

1.  [howto, Query a Web-service API for 'count' using a Simple Smoke Test Script in the provided Container-Image](#org2f42c30)
2.  [howto, Query a Web-service API for 'count' using a Simple Smoke Test Script in an adapted Container-Image](#orgcddd50c)
3.  [howto, Deploy a Simple Smoke Test Container-Image to Fargate via Pipeline](#orgf71f871)


<a id="org2f42c30"></a>

# howto, Query a Web-service API for 'count' using a Simple Smoke Test Script in the provided Container-Image

-   **context:** -   <code>[/]</code> overview
    -   info/notes
        -   on Assumptions and Prerequisites
            -   Docker (or similar) is installed and available.
        -   on Commentary
            -   This isn't a particularly elegant or favorable approach and is presented solely to demonstrate an option that dogmatically adheres to requirement req6.4.
            -   The purpose of a docker container is to create and seal a runtime, then make it portable.
            -   The runtime in the provided container-image is generic and not particularly well-suited to this task.
            -   For a more operationally favorable approach, (a customized container-image), see
            -   In order to accomplish this portion of the exercise goal in strict adherence with the requi

-   **procedure:** Return an integer count using the provided container-image
    -   Run the Container-Image from CLI to demonstrate and verify
        
            docker run -t --rm -v $(pwd)/assets/exec_script/bash__curl_getcount.bash:/run.sh ${oci_image} /run.sh


<a id="orgcddd50c"></a>

# howto, Query a Web-service API for 'count' using a Simple Smoke Test Script in an adapted Container-Image

-   **procedure:** Return an integer count using a customized container-image
    -   *(ex.)* build new multi-stage container
        
            # -*- docker-image-name: "adapted_but_undaunted" -*-
            FROM debian:testing-slim
            RUN DEBIAN_FRONTEND=noninteractive apt -y update && apt install -y curl
            COPY script.sh /run
            ENTRYPOINT ["/run"]
            
            
            ## EOF
    
    -   *(ex.)* Run the Mininal and Self-Contained Container-Image from CLI to demonstrate and verify
        
            docker run -t --rm  ${oci_image}


<a id="orgf71f871"></a>

# howto, Deploy a Simple Smoke Test Container-Image to Fargate via Pipeline

-   **procedure:** *(wip)* Deploy a Container in EKS/Fargate using AWS CLI in Gitlab CI
    -   nota bene
        -   <https://docs.gitlab.com/ee/ci/cloud_deployment/#run-aws-commands-from-gitlab-cicd>
-   **procedure:** *(wip)* Deploy a Container in EKS/Fargate using Terraform in Gitlab CI

