#!/bin/bash

#,NOTE: this Ubuntu v18.04 image contains: Bash v4.4.20, GNU sed v4.4

#,HINT: Return a -1 on error
curl -s https://api.countapi.xyz/info/04e65d95da6186f6c30e1385f6727ecb23f2585a | sed -nE 's/.*"value":([[:digit:]]+).*/\1/p' || echo -1
