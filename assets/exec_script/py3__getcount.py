# [[id:CC08573C-18E8-4C05-A9E0-3D8A12518076][howto, Re-Write the Simple Smoke Test Script in a More Robust Language:1]]
#!/usr/bin/env python3
import os
import requests

uri_endpoint = os.environ["uri_endpoint"]
count_key = os.environ["count_key"]
# Q&D
print((requests.get(f"{uri_endpoint}/{count_key}", headers={"Accept": "application/json"})).json()["value"])
# howto, Re-Write the Simple Smoke Test Script in a More Robust Language:1 ends here
